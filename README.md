# test-before
based on https://github.com/jldec/test-before

#### .gitpod.yml
```yaml
tasks:
  - before: |
      echo `gp url` >> before.txt
    command: |
      echo `gp url` >> command.txt
```

This repo demonstrates the following

1. On GitHub, A gitpod `before` task is run as part of a prebuild even if there is no `init` task. On GitLab this is not the case.
2. The `before` task is run in front of the `init` and the `command` task in the workspace
3. The workspace URL returned by `gp url` is different for the prebuild workspace.

### How to test

- Open this repo in a gitpod workspace
- `before.txt` contains 1 lines for the init, and one line for the command task
- `command.txt` contains the url of the main workspace

---
touch
touch 27  
touch 28  
touch 29  

Hello
Goodbye
